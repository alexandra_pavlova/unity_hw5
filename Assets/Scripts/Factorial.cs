using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Factorial : MonoBehaviour
{
    [SerializeField] private InputField inputField;

    public void OnClick()
    {
        int factorialResult = 1;
        int size = Convert.ToInt32(inputField.text)+1;
        int[] forFactorial = new int[size];
        for (int i = 1; i < size; i++)
        {
            factorialResult = factorialResult * i;
        }
        
        Debug.Log($"Factorial result: {factorialResult}\n");
    }

}
