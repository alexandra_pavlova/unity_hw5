using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MultiplicationTable : MonoBehaviour
{
  
    public void OnClick()
    {
		string foroutput = "";
        int[,] numbers = new int[11, 11];
        for (int i = 1; i < 11; i++)
        {
			foroutput = "";
            for (int j = 1; j < 11; j++)
            {
                numbers[i,j] = i * j;
				foroutput = foroutput + (numbers[i,j]).ToString() + " ";
            }
			foroutput = foroutput + "\n";
			Debug.Log(foroutput);
        }
    }
}
